
## Java线程创建的方法
4种
- 方式一：继承于Thread类

步骤：
1.创建一个继承于Thread类的子类
2.重写Thread类的run() --> 将此线程执行的操作声明在run()中
3.创建Thread类的子类的对象
4.通过此对象调用start()执行线程
- 方式二：实现Runnable接口
  步骤：
  1.创建一个实现了Runnable接口的类
  2.实现类去实现Runnable中的抽象方法：run()
  3.创建实现类的对象
  4.将此对象作为参数传递到Thread类的构造器中，创建Thread类的对象
  5.通过Thread类的对象调用start()
  ① 启动线程
  ②调用当前线程的run()–>调用了Runnable类型的target的run()

方式三：实现Callable接口
步骤：
1.创建一个实现Callable的实现类
2.实现call方法，将此线程需要执行的操作声明在call()中
3.创建Callable接口实现类的对象
4.将此Callable接口实现类的对象作为传递到FutureTask构造器中，创建FutureTask的对象
5.将FutureTask的对象作为参数传递到Thread类的构造器中，创建Thread对象，并调用start()
6.获取Callable中call方法的返回值

实现Callable接口的方式创建线程的强大之处

call()可以有返回值的
call()可以抛出异常，被外面的操作捕获，获取异常的信息
Callable是支持泛型的
- 方式四：使用线程池
线程池好处：
1.提高响应速度（减少了创建新线程的时间）
2.降低资源消耗（重复利用线程池中线程，不需要每次都创建）
3.便于线程管理
  核心参数：

corePoolSize：核心池的大小
maximumPoolSize：最大线程数
keepAliveTime：线程没有任务时最多保持多长时间后会终止
步骤：
1.以方式二或方式三创建好实现了Runnable接口的类或实现Callable的实现类
2.实现run或call方法
3.创建线程池
4.调用线程池的execute方法执行某个线程，参数是之前实现Runnable或Callable接口的对象

```shell
package com.atguigu.java2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

class NumberThread implements Runnable {
    @Override
    public void run() {
        //遍历100以内的偶数
        for (int i = 0; i <= 100; i++) {
            if (i % 2 == 0) {
                System.out.println(Thread.currentThread().getName() + ": " + i);
            }
        }
    }
}

class NumberThread1 implements Runnable {
    @Override
    public void run() {
        //遍历100以内的奇数
        for (int i = 0; i <= 100; i++) {
            if (i % 2 != 0) {
                System.out.println(Thread.currentThread().getName() + ": " + i);
            }
        }
    }
}


public class ThreadPool {

    public static void main(String[] args) {
        //1. 提供指定线程数量的线程池
        ExecutorService service = Executors.newFixedThreadPool(10);

        //输出class java.util.concurrent.ThreadPoolExecutor
        System.out.println(service.getClass());

        ThreadPoolExecutor service1 = (ThreadPoolExecutor) service;
        //自定义线程池的属性
//        service1.setCorePoolSize(15);
//        service1.setKeepAliveTime();

        //2. 执行指定的线程的操作。需要提供实现Runnable接口或Callable接口实现类的对象
        service.execute(new NumberThread());//适用于Runnable
        service.execute(new NumberThread1());//适用于Runnable
//        service.submit(Callable callable);//适合使用于Callable

        //3. 关闭连接池
        service.shutdown();
    }

}


```
- 方式五：使用匿名类
```Thread thread = new Thread(new Runnable() {
@Override
public void run() {
// 线程需要执行的任务代码
System.out.println("子线程开始启动....");
for (int i = 0; i < 30; i++) {
System.out.println("run i:" + i);
}
}
});
thread.start();
````````
或者
```
new Thread(() -> {
System.out.println(Thread.currentThread().getName() + "\t上完自习，离开教室");
}, "MyThread").start();
``````
## JVM 线程状态？
6种线程
![img_1.png](img_1.png)


## 线程中止中止的方法？
结束run方法，return 和抛出异常都可以

- 使用标志位终止线程
在 run() 方法执行完毕后，该线程就终止了。但是在某些特殊的情况下，run() 方法会被一直执行；比如在服务端程序中可能会使用 while(true) { ... } 这样的循环结构来不断的接收来自客户端的请求。此时就可以用修改标志位的方式来结束 run() 方法。
 ``` public class ServerThread extends Thread {
  //volatile修饰符用来保证其它线程读取的总是该变量的最新的值
  public volatile boolean exit = false;

  @Override
  public void run() {
  ServerSocket serverSocket = new ServerSocket(8080);
  while(!exit){
  serverSocket.accept(); //阻塞等待客户端消息
  ...
  }
  }

  public static void main(String[] args) {
  ServerThread t = new ServerThread();
  t.start();
  ...
  t.exit = true; //修改标志位，退出线程
  }
  }

```
- 2. 使用 stop() 终止线程
 为什么弃用stop：

调用 stop() 方法会立刻停止 run() 方法中剩余的全部工作，包括在 catch 或 finally 语句中的，并抛出ThreadDeath异常(通常情况下此异常不需要显示的捕获)，因此可能会导致一些清理性的工作的得不到完成，如文件，数据库等的关闭。
调用 stop() 方法会立即释放该线程所持有的所有的锁，导致数据得不到同步，出现数据不一致的问题。
- 3. 使用 interrupt() 中断线程
     使用 interrupt() 中断线程
     现在我们知道了使用 stop() 方式停止线程是非常不安全的方式，那么我们应该使用什么方法来停止线程呢？答案就是使用 interrupt() 方法来中断线程。

需要明确的一点的是：interrupt() 方法并不像在 for 循环语句中使用 break 语句那样干脆，马上就停止循环。调用 interrupt() 方法仅仅是在当前线程中打一个停止的标记，并不是真的停止线程。

也就是说，线程中断并不会立即终止线程，而是通知目标线程，有人希望你终止。至于目标线程收到通知后会如何处理，则完全由目标线程自行决定。这一点很重要，如果中断后，线程立即无条件退出，那么我们又会遇到 stop() 方法的老问题。

事实上，如果一个线程不能被 interrupt，那么 stop 方法也不会起作用。

我们来看一个使用 interrupt() 的例子：
```shell
public class InterruptThread1 extends Thread{

    public static void main(String[] args) {
        try {
            InterruptThread1 t = new InterruptThread1();
            t.start();
            Thread.sleep(200);
            t.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        super.run();
        for(int i = 0; i <= 200000; i++) {
            System.out.println("i=" + i);
        }
    }
    
}

```
从输出的结果我们会发现 interrupt 方法并没有停止线程 t 中的处理逻辑，也就是说即使 t 线程被设置为了中断状态，但是这个中断并不会起作用，那么该如何停止线程呢？

这就需要使用到另外两个与线程中断有关的方法了：
```shell
public boolean Thread.isInterrupted() //判断是否被中断
public static boolean Thread.interrupted() //判断是否被中断，并清除当前中断状态

```
这两个方法使得当前线程能够感知到是否被中断了（通过检查标志位）。

所以如果希望线程 t 在中断后停止，就必须先判断是否被中断，并为它增加相应的中断处理代码：

```

@Override
public void run() {
    super.run();
    for(int i = 0; i <= 200000; i++) {
        //判断是否被中断
        if(Thread.currentThread().isInterrupted()){
            //处理中断逻辑
            break;
        }
        System.out.println("i=" + i);
    }
}
```
在上面这段代码中，我们增加了 Thread.isInterrupted() 来判断当前线程是否被中断了，如果是，则退出 for 循环，结束线程。

这种方式看起来与之前介绍的“使用标志位终止线程”非常类似，但是在遇到 sleep() 或者 wait() 这样的操作，我们只能通过中断来处理了。

public static native void sleep(long millis) throws InterruptedException

Thread.sleep() 方法会抛出一个 InterruptedException 异常，当线程被 sleep() 休眠时，如果被中断，这会就抛出这个异常。
（注意：Thread.sleep() 方法由于中断而抛出的异常，是会清除中断标记的。）
##  Java sleep wait 方法