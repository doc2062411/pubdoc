## java
- java基础
- 数据结构

## 中
- 多线程高并发
- 设计模式


## 高
- spring
- springboot
- springmvc

## 初
- go
  
## 前端
- vue
- html 
- css 
- js

## 数据持久
- mysql
- es+kibana
- redis

## 异步mq
- kafa

