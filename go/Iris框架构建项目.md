#Iris框架构建项目

## 建立项目目录

```shell
mkdir IrisBlog 
cd IrisBlog
```

## 初始化项目

```shell
C:\Users\liuyue\www\iriblog>go mod init IrisBlog
go: creating new go.mod: module IrisBlog
```

## 配置国内安装源

```shell
go env -w GOPROXY=https://goproxy.cn,direct
```

## 安装彩虹女神

```shell
go get github.com/kataras/iris/v12@master
```

系统返回

```shell
C:\Users\liuyue\www\iriblog>go get -u github.com/kataras/iris
go: downloading github.com/kataras/iris v0.0.2
go: downloading github.com/BurntSushi/toml v0.3.1
go: downloading github.com/kataras/golog v0.0.18
go: downloading github.com/kataras/pio v0.0.8
go: downloading github.com/kataras/sitemap v0.0.5
go: downloading github.com/BurntSushi/toml v1.2.0
go: downloading github.com/kataras/tunnel v0.0.1
go: downloading github.com/kataras/golog v0.1.7
go: downloading github.com/kataras/pio v0.0.10
go: downloading gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
go: downloading github.com/kataras/tunnel v0.0.4
go: downloading gopkg.in/yaml.v3 v3.0.1
go: downloading github.com/Shopify/goreferrer v0.0.0-20181106222321-ec9c9a553398
go: downloading github.com/fatih/structs v1.1.0
go: downloading github.com/andybalholm/brotli v1.0.1-0.20200619015827-c3da72aa01ed
go: downloading github.com/iris-contrib/schema v0.0.2
go: downloading github.com/andybalholm/brotli v1.0.4
go: downloading github.com/iris-contrib/schema v0.0.6
go: downloading github.com/json-iterator/go v1.1.10
go: downloading github.com/Shopify/goreferrer v0.0.0-20220729165902-8cddb4f5de06
go: downloading github.com/klauspost/compress v1.10.10
go: downloading github.com/klauspost/compress v1.15.9
go: downloading github.com/microcosm-cc/bluemonday v1.0.3
go: downloading github.com/russross/blackfriday/v2 v2.0.1
go: downloading github.com/vmihailenco/msgpack/v5 v5.0.0-beta.1
go: downloading golang.org/x/net v0.0.0-20200707034311-ab3426394381
go: downloading github.com/russross/blackfriday v1.5.2
go: downloading github.com/vmihailenco/msgpack v4.0.4+incompatible
go: downloading github.com/microcosm-cc/bluemonday v1.0.19
go: downloading github.com/vmihailenco/msgpack/v5 v5.3.5
go: downloading golang.org/x/text v0.3.3
go: downloading golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e
go: downloading github.com/russross/blackfriday/v2 v2.1.0
go: downloading github.com/russross/blackfriday v1.6.0
go: downloading google.golang.org/protobuf v1.25.0
go: downloading golang.org/x/time v0.0.0-20220722155302-e5dcc9cfc0b9
go: downloading golang.org/x/net v0.0.0-20220812174116-3211cb980234
go: downloading google.golang.org/protobuf v1.28.1
go: downloading golang.org/x/text v0.3.7
go: downloading golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
go: downloading golang.org/x/sys v0.0.0-20200808120158-1030fc2bf1d9
go: downloading github.com/schollz/closestmatch v2.1.0+incompatible
go: downloading golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
go: downloading golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab
go: downloading gopkg.in/ini.v1 v1.57.0
go: downloading gopkg.in/ini.v1 v1.67.0
go: downloading github.com/ryanuber/columnize v2.1.0+incompatible
go: downloading github.com/CloudyKit/jet/v4 v4.1.0
go: downloading github.com/aymerick/raymond v2.0.3-0.20180322193309-b565731e1464+incompatible
go: downloading github.com/eknkc/amber v0.0.0-20171010120322-cdade1c07385
go: downloading github.com/ryanuber/columnize v2.1.2+incompatible
go: downloading github.com/iris-contrib/jade v1.1.4
go: downloading github.com/CloudyKit/jet v2.1.2+incompatible
go: downloading github.com/iris-contrib/pongo2 v0.0.1
go: downloading github.com/kataras/blocks v0.0.2
go: downloading github.com/yosssi/ace v0.0.5
go: downloading github.com/kataras/blocks v0.0.6
go: downloading github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742
go: downloading github.com/chris-ramon/douceur v0.2.0
go: downloading github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd
go: downloading github.com/vmihailenco/tagparser v0.1.1
go: downloading google.golang.org/appengine v1.6.5
go: downloading github.com/vmihailenco/tagparser v0.1.2
go: downloading github.com/shurcooL/sanitized_anchor_name v1.0.0
go: downloading google.golang.org/appengine v1.6.7
go: downloading github.com/google/uuid v1.1.2-0.20200519141726-cb32006e483f
go: downloading github.com/google/uuid v1.3.0
go: downloading github.com/CloudyKit/fastprinter v0.0.0-20200109182630-33d98a066a53
go: downloading github.com/valyala/bytebufferpool v1.0.0
go: downloading github.com/aymerick/douceur v0.2.0
go: downloading github.com/gorilla/css v1.0.0
go: downloading github.com/golang/protobuf v1.4.1
go: downloading github.com/golang/protobuf v1.5.2
go: downloading github.com/vmihailenco/tagparser/v2 v2.0.0
go: added github.com/BurntSushi/toml v1.2.0
go: added github.com/CloudyKit/fastprinter v0.0.0-20200109182630-33d98a066a53
go: added github.com/CloudyKit/jet/v4 v4.1.0
go: added github.com/Shopify/goreferrer v0.0.0-20220729165902-8cddb4f5de06
go: added github.com/andybalholm/brotli v1.0.4
go: added github.com/aymerick/douceur v0.2.0
go: added github.com/aymerick/raymond v2.0.3-0.20180322193309-b565731e1464+incompatible
go: added github.com/chris-ramon/douceur v0.2.0
go: added github.com/eknkc/amber v0.0.0-20171010120322-cdade1c07385
go: added github.com/fatih/structs v1.1.0
go: added github.com/golang/protobuf v1.5.2
go: added github.com/google/uuid v1.3.0
go: added github.com/gorilla/css v1.0.0
go: added github.com/iris-contrib/jade v1.1.4
go: added github.com/iris-contrib/pongo2 v0.0.1
go: added github.com/iris-contrib/schema v0.0.6
go: added github.com/json-iterator/go v1.1.12
go: added github.com/kataras/blocks v0.0.6
go: added github.com/kataras/golog v0.1.7
go: added github.com/kataras/iris v0.0.2
go: added github.com/kataras/pio v0.0.10
go: added github.com/kataras/sitemap v0.0.5
go: added github.com/kataras/tunnel v0.0.4
go: added github.com/klauspost/compress v1.15.9
go: added github.com/microcosm-cc/bluemonday v1.0.19
go: added github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd
go: added github.com/modern-go/reflect2 v1.0.2
go: added github.com/russross/blackfriday/v2 v2.1.0
go: added github.com/ryanuber/columnize v2.1.2+incompatible
go: added github.com/schollz/closestmatch v2.1.0+incompatible
go: added github.com/shurcooL/sanitized_anchor_name v1.0.0
go: added github.com/valyala/bytebufferpool v1.0.0
go: added github.com/vmihailenco/msgpack/v5 v5.3.5
go: added github.com/vmihailenco/tagparser v0.1.2
go: added github.com/vmihailenco/tagparser/v2 v2.0.0
go: added github.com/yosssi/ace v0.0.5
go: added golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
go: added golang.org/x/net v0.0.0-20220812174116-3211cb980234
go: added golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab
go: added golang.org/x/text v0.3.7
go: added golang.org/x/time v0.0.0-20220722155302-e5dcc9cfc0b9
go: added google.golang.org/appengine v1.6.7
go: added google.golang.org/protobuf v1.28.1
go: added gopkg.in/ini.v1 v1.67.0
go: added gopkg.in/yaml.v3 v3.0.1
```

    安装完毕之后，可以打开项目中go.mod文件查看Iris的依赖列表：

```shell
module go-yuan

go 1.18

require (
    github.com/BurntSushi/toml v1.2.1 // indirect
    github.com/CloudyKit/fastprinter v0.0.0-20200109182630-33d98a066a53 // indirect
    github.com/CloudyKit/jet/v6 v6.2.0 // indirect
    github.com/Joker/jade v1.1.3 // indirect
    github.com/Shopify/goreferrer v0.0.0-20220729165902-8cddb4f5de06 // indirect
    github.com/andybalholm/brotli v1.0.5 // indirect
    github.com/aymerick/douceur v0.2.0 // indirect
    github.com/eknkc/amber v0.0.0-20171010120322-cdade1c07385 // indirect
    github.com/fatih/structs v1.1.0 // indirect
    github.com/flosch/pongo2/v4 v4.0.2 // indirect
    github.com/golang/snappy v0.0.4 // indirect
    github.com/google/uuid v1.3.0 // indirect
    github.com/gorilla/css v1.0.0 // indirect
    github.com/iris-contrib/schema v0.0.6 // indirect
    github.com/josharian/intern v1.0.0 // indirect
    github.com/kataras/blocks v0.0.7 // indirect
    github.com/kataras/golog v0.1.8 // indirect
    github.com/kataras/iris/v12 v12.2.0-beta7.0.20230219194850-dccd57263617 // indirect
    github.com/kataras/pio v0.0.11 // indirect
    github.com/kataras/sitemap v0.0.6 // indirect
    github.com/kataras/tunnel v0.0.4 // indirect
    github.com/klauspost/compress v1.15.15 // indirect
    github.com/mailgun/raymond/v2 v2.0.48 // indirect
    github.com/mailru/easyjson v0.7.7 // indirect
    github.com/microcosm-cc/bluemonday v1.0.22 // indirect
    github.com/russross/blackfriday/v2 v2.1.0 // indirect
    github.com/schollz/closestmatch v2.1.0+incompatible // indirect
    github.com/sirupsen/logrus v1.8.1 // indirect
    github.com/tdewolff/minify/v2 v2.12.4 // indirect
    github.com/tdewolff/parse/v2 v2.6.4 // indirect
    github.com/valyala/bytebufferpool v1.0.0 // indirect
    github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
    github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
    github.com/yosssi/ace v0.0.5 // indirect
    golang.org/x/crypto v0.6.0 // indirect
    golang.org/x/net v0.7.0 // indirect
    golang.org/x/sys v0.5.0 // indirect
    golang.org/x/text v0.7.0 // indirect
    golang.org/x/time v0.3.0 // indirect
    google.golang.org/protobuf v1.28.1 // indirect
    gopkg.in/ini.v1 v1.67.0 // indirect
    gopkg.in/yaml.v3 v3.0.1 // indirect
)
```

项目目录

```go
package main

import "github.com/kataras/iris"

func main() {
	app := iris.New()
	app.Use(iris.Compression)

	app.Get("/", func(ctx iris.Context) {
		ctx.HTML("你好 <strong>%s</strong>!", "女神")
	})

	app.Listen(":5000")
}
```

随后在终端启动Iris服务：

```
go run main.go
```
