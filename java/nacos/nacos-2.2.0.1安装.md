# Nacos-2.2.0.1安装

参考文档
https://nacos.io/zh-cn/docs/quick-start.html

## 下载nacos
```shell
https://github.com/alibaba/nacos/releases/download/2.2.0.1/nacos-server-2.2.0.1.zip
```
## 解压安装
```shell
tar -xvf nacos-server-$version.tar.gz
```
## 启动服务器
注：Nacos的运行需要以至少2C4g60g*3的机器配置下运行。
Linux/Unix/Mac
启动命令(standalone代表着单机模式运行，非集群模式):

sh startup.sh -m standalone

如果您使用的是ubuntu系统，或者运行脚本报错提示[[符号找不到，可尝试如下运行：

bash startup.sh -m standalone

Windows
启动命令(standalone代表着单机模式运行，非集群模式):

startup.cmd -m standalone