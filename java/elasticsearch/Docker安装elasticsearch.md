# Docker安装ES
## 安装ES
### 拉取ES镜像
```shell
 docker pull elasticsearch:7.4.2
```
### 挂载数据
```shell
# 创建目录
mkdir -p /home/es/config
mkdir -p /home/es/data/
mkdir -p /home/es/plugins
# 配置es
echo "http.host: 0.0.0.0" >> /home/es/config/elasticsearch.yml
#  修改文件权限
chmod -R 777 /home/es/config
chmod -R 777 /home/es/data/
chmod -R 777  /home/es/plugins
```
### 启动ES
```shell
docker run --name elasticsearch \
-p 9200:9200 -p 9300:9300  \
-e "discovery.type=single-node" -e ES_JAVA_OPTS="-Xms64m -Xmx128m" \
-v /home/es/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml  \
-v /home/es/data/:/usr/share/elasticsearch/data  \
-v /home/es/plugins:/usr/share/elasticsearch/plugins  \
-d elasticsearch:7.4.2
```
在浏览器上输入IP和es默认的端口号9200。浏览器返回的数据跟下图一样，则表示成功了。

![img_2.png](img_2.png)
```shell
docker exec -it elasticsearch /bin/bash
```
```shell
./bin/elasticsearch-plugin install  \
https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.4.2/elasticsearch-analysis-ik-7.4.2.zip
```

## 安装Kibana
### 准备
终端输入docker inspect elasticsearch获取ES对外的IP
```shell
docker inspect elasticsearch
```
![img_1.png](img_1.png)
### 启动Kibana
<font color='red'>注意</font>：es地址用上面查询的
```
sudo docker run --name kibana   \
-e ELASTICSEARCH_HOSTS=http://172.17.0.2:9200  \
 -p 5601:5601 -d kibana:7.4.2
```
### 配置Kibana
```shell
docker exec -it kibana /bin/bash
```
```shell
#进入config目录
cd config
#修改kibana.yml文件
vi kibana.yml


elasticsearch.hosts: [ "http://172.17.0.2:9200" ]
```
在浏览器输入IP:5601测试，下图是系统的界面。
![img.png](img.png)
