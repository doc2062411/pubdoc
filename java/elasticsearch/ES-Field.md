# ES-Fields
## 核心数据类型

<hr>

### 1) 字符串 text，keyword
#### text
- a. 支持分词、全文检索、模糊、精确查询，不支持聚合，排序操作；
- b. 最大字符字符长度无限制，适合大字段存储；
应用场景：
  存储全文数据，邮箱内容，地址，代码块，博客文章内容等。
  默认结合standard analyzer(标准解析器)对文本进行分词、倒排索引。
  默认结合标准分析器进行词命中、词频相关度打分。
```shell
{
  "f1": {
    "type": "text"
  }
}
```
<hr>

####  keyword
- a. 不进行分词，直接索引、支持模糊、支持精确匹配，支持聚合、排序操作。
- b.  keyword类型的最大支持的长度为——32766个UTF-8类型的字符,可以通过设置ignore_above指定自持字符长度，超过给定长度后的数据将不被索引，无法通过term精确匹配检索返回结果。
  使用场景：
  存储邮箱号码、url、name、title，手机号码、主机名、状态码、邮政编码、标签、年龄、性别等数据。
  用于筛选数据(例如: select * from x where status='open')、排序、聚合(统计)。
  直接将完整的文本保存到倒排索引中。
```shell
{
  "f1": {
    "type": "keyword"
  }
}

```
<hr>

### 2) 数值类型
#### byte
8位有符号整数，范围[-128-127]
````shell
{
"type": "byte"
}
````
#### short
16位有符号整数，范围[-32768-32767]
````shell
{
"type": "short"
}
````
#### integer 
有符号的32位整数
```shell
{
  "type": "integer"
}
```
#### long
有符号的64位整数
````shell
{
"type": "long"
}
````
#### float
32位单精度浮点数
````shell
{
"type": "long"
}
````

#### double
64位双精度浮点数
````shell
{
"type": "double"
}
````

#### half_float
16位半精度IEEE 754浮点类型
````shell
{
"type": "half_float"
}
````
#### scaled_float
缩放类型的的浮点数, 比如price字段只需精确到分, 57.34缩放因子为100, 存储结果为5734
```shell
 {
   "type": "scaled_float",
   "scaling_factor": 100
}
```
<hr>

### 3) boolean、binary类型
#### boolean
可以使用boolean类型的（true、false）也可以使用string类型的（“true”、“false”）。
```shell
{
  "type": "boolean"
}
```
#### binary类型：

二进制类型是Base64编码字符串的二进制值，不以默认的方式存储，且不能被搜索
```shell
{
  "type": "binary"
}
```
<hr>

### 4) 日期类型
- 使用format指定格式：

若未指定格式，则使用默认格式: strict_date_optional_time||epoch_millis
```shell
{
  "type": "date" 
}
```
- 指定多个format：

使用双竖线||分隔指定多种日期格式，每个格式都会被依次尝试，直到找到匹配的
```shell
{
  "type":   "date",
  "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
}
```
### 5) 范围类型
<hr>

## 复杂数据类型


### object
### array
### nested
### flattened
### join

<hr>

## 专用数据类型
### geo_point
### geo_shape
### point、shape   

<hr>