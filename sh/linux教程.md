[toc]
# linux 教程

## Shell 变量
变量名不能加美元符号$（$ php语言变量需要）
```shell
name="abel"
```
注意：变量名和等号之间不能有空格
变量名规则：
- 只能使用英文字母，数字，下划线，首字母不能使用数字
- 中间不能使用空格
- 不能使用标点符号
- 不能使用bash关键字（help命令查看保留关键字）
### 使用变量
使用一个定义过的变量，只要在变量名前加上$符号即可
```shell
your_name="qq"
echo $your_name
echo ${your_name}
```
### 只读变量
```shell
myurl="www.baidu.com"
readonly myurl
myurl="www.hh.com"
```
运行脚本，结果如下：
```shell
/bin/sh: NAME: This variable is read only.
```
### 删除变量
使用unset删除变量，变量删除后不能再次使用，unset命令不能删除只读变量
```shell
#! /bin/bash

myUrl="www.baidu.com"
unset myUrl
echo $myUrl
# 没有运行结果
```

### 变量类型
运行shell时，会同时存在三种变量：

1) 局部变量 局部变量在脚本或命令中定义，仅在当前shell实例中有效，其他shell启动的程序不能访问局部变量。
2) 环境变量 所有的程序，包括shell启动的程序，都能访问环境变量，有些程序需要环境变量来保证其正常运行。必要的时候shell脚本也可以定义环境变量。
3) shell变量 shell变量是由shell程序设置的特殊变量。shell变量中有一部分是环境变量，有一部分是局部变量，这些变量保证了shell的正常运行

