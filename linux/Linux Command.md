# Linux Operation Command

## instal and login command
- login
- shutdown
- halt
- rebot
- install
- mount
- unmount
- chsh
- exit
- last
## file  processing commands
- file
- mkdir
- grep
- dd
- find
- mv
- ls
- diff
- cat
- ln
## system management commands
- df
- top 
- free
- quota 
- at 
- lp
- adduser
- groupadd
- kill
- crontab
## network operation commands
- ifconfig
- ip
- ping
- netstat
- telnet 
- ftp
- route 
- rlogin
- rcp
- finger
- mail
- nslookup
## System Security Related Commands
- passwd
- su
- umask
- chgrp
- chmod
- chown
- chattr
- sudo ps
- who
## other commands
- tar
- unzip
- gunzip
- unarj
- mtools
- man
- unendcode
- uudecode

## change privilege of file/index
### chmod

change mode

linux/unix file call permission are divide into three levels:
- owner 
- Group
- other Users

 [`-` or d ][rwx][rwx]
7  rwx  421

grammer:
```shell
chmod [-cfvR] [--help][--version]  file/index
```
```
chmod u+x g=rwx o+x  cur.txt
chmod 777 cur.txt
chmod  -R 751 cur.txt
```
a  equals ugo
 
## Linux file and index management
- absolute path
   from / start, /usr/share/doc
- relative path
   ../share/doc  ./doc
### Common Commands
- ls ->list files
- cd -> change directory
- pwd -> print work directory
- mkdir -> make directory 
- rmdir -> remove directory
- cp -> copy  file or index
- rm -> romve file or index
- mv ->move file and  index,or change the name of file and index
we can use `man`[command] to show the document of every command ,like man cp